package com.pariosoft.prep.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.pariosoft.prep.DatabaseClasses.UserEvent;
import com.pariosoft.prep.R;

import java.util.List;

/**
 * Created by mereo on 2017-01-15.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //Bref VIEWHOLDER permet d'afficher l'information de la liste d'événement
        public TextView titleTextView;
        List<UserEvent> userEvent;
        Context context;

        public ViewHolder(Context context, View itemView, List<UserEvent> userEvents) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.list_event_title);
            itemView.setOnClickListener(this);
            this.userEvent = userEvents;
            this.context = context;

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                Toast.makeText(context, userEvent.get(position).getEventKey(),Toast.LENGTH_LONG)
                        .show();
            }
        }
    }

    //List of all UserEvents
    List<UserEvent> mUserEvents;
    //The context
    private Context mContext;

    //Constructor
    public EventsAdapter(List<UserEvent> mUserEvents, Context mContext) {
        this.mUserEvents = mUserEvents;
        this.mContext = mContext;
    }

    public Context getContext(){
        return mContext;
    }

    //Inflates the Event list layout and create the holder... then returns it
    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        //We inflate the customLayout
        View userEventView = inflater.inflate(R.layout.event_list, parent,false);

        //we return a new Holder based on the customView
        ViewHolder viewHolder = new ViewHolder(getContext(), userEventView, mUserEvents);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder holder, int position) {
        holder.titleTextView.setText(mUserEvents.get(position).getEvent().getTitle());
    }

    @Override
    public int getItemCount() {
        return mUserEvents.size();
    }
}
