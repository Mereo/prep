package com.pariosoft.prep;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.pariosoft.prep.DatabaseClasses.DatabaseHelper;
import com.pariosoft.prep.DatabaseClasses.Events;
import com.pariosoft.prep.DatabaseClasses.Time;

import java.util.Date;

public class NewEventActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    //Firebase Instance Variables
    private FirebaseUser mFirebaseUser;
    private GoogleApiClient mGoogleApiClient;

    //TODO CLEAN UP THIS SHIT!!!

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        }

        //GOOGLE API FOR PLACES
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        //PLACES AUTOCOMPLETE TEST
//        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().
//                setTypeFilter(Place.TYPE_COUNTRY).setCountry("CA").build();
//        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
//                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
//        autocompleteFragment.setFilter(typeFilter);

        Button customToolbarSaveButton = (Button) findViewById(R.id.customViewSaveButton);
        customToolbarSaveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                TextView title = (TextView)findViewById(R.id.editTitle);
                TextView address = (TextView)findViewById(R.id.editAddress);
                TextView description = (TextView)findViewById(R.id.editDescription);
                TextView editRemarks = (TextView)findViewById(R.id.editRemarks);
                DatePicker mDatePicker = (DatePicker)findViewById(R.id.datePicker);
                TimePicker mTimePicker = (TimePicker)findViewById(R.id.timePicker);

                int year = mDatePicker.getYear();
                int month = mDatePicker.getMonth();
                int day = mDatePicker.getDayOfMonth();

                Date d = new Date(year, month, day);

                Time time = new Time();
                time.setHour(mTimePicker.getCurrentHour());
                time.setMinute(mTimePicker.getCurrentMinute());


                DatabaseHelper.createEvent(mFirebaseUser, new Events(
                        title.getText().toString(),
                        "",
                        address.getText().toString(),
                        description.getText().toString(),
                        editRemarks.getText().toString(),
                        d,
                        time
                ));
                finish();
            }

        });





//
//
// FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    /**
     * We want the back button to return back
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
