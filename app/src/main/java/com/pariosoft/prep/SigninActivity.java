package com.pariosoft.prep;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.pariosoft.prep.DatabaseClasses.DatabaseHelper;
import com.pariosoft.prep.utils.GeneralMethods;

import static android.Manifest.permission.READ_PHONE_STATE;

public class SignInActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final String TAG = "SignInActivity";
    private static final int RC_SIGN_IN = 9001;

    private SignInButton mSignInButton;

    private GoogleApiClient mGoogleApiClient;

    private String mPhoneNumber = "";
    private ProgressDialog signinProgressDialog;

    private static final int REQUEST_CODE_PHONE_STATE =25;

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        getPhoneNumber();
        initialiseFirebase();
        actionBar.hide();


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    private void initialiseFirebase() {
        // Initialise FireBase
        mFirebaseAuth  = FirebaseAuth.getInstance();

        mSignInButton = (SignInButton) findViewById(R.id.sign_in_button);

        // Set click listeners
        mSignInButton.setOnClickListener(this);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder
                (GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Result returned from launching the Inntent from Google.getSigninIntent(..);
        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
        if (result.isSuccess()) {
            // Google siginin was successful, authenticate with Firebase
            GoogleSignInAccount account = result.getSignInAccount();

            //Progress dialog
            signinProgressDialog = new ProgressDialog(this);
            signinProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            signinProgressDialog.setTitle(getString(R.string.sign_in_progress_dialog_title));
            signinProgressDialog.setMessage(getString(R.string.sign_in_progress_dialog_message));
            signinProgressDialog.setIndeterminate(true);
            signinProgressDialog.setCanceledOnTouchOutside(false);
            signinProgressDialog.show();
            getFireBaseAuthWithGoogle(account);
        } else {
            // Google sign in failed
            Log.e(TAG, "Google sign in failed");
            //signinProgressDialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(SignInActivity.this);
            builder.setTitle(getString(R.string.google_sign_in_failed_title));
            builder.setMessage(getString(R.string.google_sign_in_failed_message));
            builder.setPositiveButton(getString(R.string.btn_ok), null);
            builder.show();
        }
    }

    private void getFireBaseAuthWithGoogle(GoogleSignInAccount account) {
        Log.d(TAG, "FirebaseAuthWithGoogle: " + account.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential).addOnCompleteListener(this,
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            if(signinProgressDialog.isShowing()) {
                                signinProgressDialog.dismiss();
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder
                                    (SignInActivity.this);
                            builder.setTitle(getString(R.string.authentication_failed_title));
                            builder.setMessage(getString(R.string.authentication_failed_message));
                            builder.setPositiveButton(getString(R.string.btn_ok), null);
                            builder.show();
                              Log.w(TAG, "SignInWithCredential", task.getException());
                        } else {
                            DatabaseHelper.addUser(mFirebaseAuth.getCurrentUser(), mPhoneNumber);
                            startActivity(new Intent(SignInActivity.this, StartingActivity.class));
                            signinProgressDialog.dismiss();
                            finish();
                        }
                    }
                });
    }


    private void getPhoneNumber() {

       int permissionCheck = ContextCompat.checkSelfPermission(this, READ_PHONE_STATE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.permission_phone_number));
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.btn_ok),                       new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityCompat.requestPermissions(SignInActivity.this, new String[]
                        {READ_PHONE_STATE}, REQUEST_CODE_PHONE_STATE);
            }
        });
        builder.show();

    } else {
//         // you have the permission
        mPhoneNumber = GeneralMethods.getPhoneNumber(this);
        if (mPhoneNumber == null) {
            mPhoneNumber = "TEST1234";
        }
     }
    }

    private void progressDialog(String message){

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mPhoneNumber = GeneralMethods.getPhoneNumber(this);
//                    if (mPhoneNumber == null) {
//                        mPhoneNumber = "TEST1234";
//                    }

                } else {
                    //Since the user has not given permission, shutdown app
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                //mUsername = ANONYMOUS;
                startActivity(new Intent(this, SignInActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}