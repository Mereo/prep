package com.pariosoft.prep.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.pariosoft.prep.R;

/**
 * Created by mereo on 2016-10-06.
 */

public class CustomPreferences {

    //Variables
    private static CustomPreferences instance = null;
    private SharedPreferences preferences;

    //Constructor
    private CustomPreferences() {
        //Since it's private, no will be to initialize this class
    }

    public static CustomPreferences getInstance() {
        //And now, we have to check if there's an instance of this Class
        //if not, we'll create the class
        if (instance == null) {
            instance = new CustomPreferences();
        }

        return instance;
    }

    private void initiatePreferences(Context context){
        preferences = context.getSharedPreferences(context.getString(R.string.app_pref_token),
                Context.MODE_PRIVATE);
    }

    public void putPhoneNumber(Context context, String string) {
        initiatePreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(context.getString(R.string.app_pref_token), string);
        editor.commit();
    }

    public String getPhoneNumber(Context context) {
        initiatePreferences(context);
        String toReturn = preferences.getString(context.getResources().getString(R.string.app_pref_token), "");
        return toReturn;
    }

}