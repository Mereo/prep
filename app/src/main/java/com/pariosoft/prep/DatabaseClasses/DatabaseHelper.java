package com.pariosoft.prep.DatabaseClasses;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by mereo on 2016-12-27.
 */

public class DatabaseHelper {

    //Constant variables
    private static final String USERS = "users";
    private static final String PHONENUMBER = "phoneNumber";
    private static final String EVENTS = "events";
    private static final String ADMINISTRATOR = "admnistrator";
    private static final String EventMember = "members";
    private static final String CHATMESSAGES = "chatMessages";
    private static final String MAP = "map";
    private static final String TIMESTAMP = "TIMESTAMP";
    //private static final String

    //Firebase instace variables
    private static DatabaseReference mFirebaseReference = FirebaseDatabase.getInstance().getReference();

//    public DatabaseHelper() {
//        //Get instance of FirebaseDatabase when constructing the
//        mFirebaseReference = FirebaseDatabase.getInstance().getReference();
//    }

//    //Method to add user, this is just a TEST
//    public boolean addUser(String uid, Users users) {
//        mFirebaseReference.child("users").child(uid).setValue(users);
//        return true;
//    }

    public Users getUserInfo() {
        return getUserInfo();
    }

    public static void addUser(final FirebaseUser firebaseUser, final String phoneNumber) {
        final Query databaseQuery = mFirebaseReference.child(StaticNames.USERS).child(firebaseUser.getUid());
        databaseQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.hasChildren()) {
                    //Adding the user in the USERS child
                    mFirebaseReference.child(StaticNames.USERS).child(firebaseUser.getUid()).setValue(
                            new Users(firebaseUser.getUid(), firebaseUser.getEmail(),
                                    firebaseUser.getDisplayName(), phoneNumber));
                    //Also adding the phone number in the phoneNumber Child
                    mFirebaseReference.child(StaticNames.PHONENUMBER).child(phoneNumber)
                            .setValue(firebaseUser.getUid());
                } else {
                    //Do nothing
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void createEvent(FirebaseUser user, Events event) {
        DatabaseReference refEvent = mFirebaseReference.child(StaticNames.EVENTS).push();
        refEvent.setValue(new Events(event.getTitle(), event.getCountry(), event.getAddress(),
                event.getDescription(), event.getRemarks(), event.getDate(), event.getTime()));
        //Firebase TIMESTAMP
        refEvent.child(StaticNames.TIMESTAMP).setValue(ServerValue.TIMESTAMP);

        String key = refEvent.getKey();

        //Now we need to put this event at the USER SCHEME and put the user as an administrator
        mFirebaseReference.child(StaticNames.USERS).child(user.getUid()).child(StaticNames.EVENTS)
                .child(key).child(StaticNames.ADMINISTRATOR)
                .setValue(true);
    }

    public static void getEvent(FirebaseUser user) {
        final Query userEventQuery = mFirebaseReference.child(StaticNames.USERS).child(user.getUid())
                .child(StaticNames.EVENTS);
        userEventQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }



//    public static void createScheme() {
//
//        //We'll create ehe Event Scheme
//        DatabaseReference newRef = mFirebaseReference.child(EVENTS).push();
//        newRef.setValue(new Events("","","","",""));
//        String key = newRef.getKey();
//        Log.d("TEST KEY", key);
//    }

//    private void getEvents() {
//        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
//        mFirebaseReference = FirebaseDatabase.getInstance().getReference();
//        final Query userEventQuery = mFirebaseReference.child(StaticNames.USERS)
//                .child(mFirebaseUser.getUid()).child((StaticNames.EVENTS));
//        userEventQuery.addListenerForSingleValueEvent(new ValueEventListener() {
//            String key;
//            boolean administrator = false;
//            UserEvent userEvent;
//            Events events = null;
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if (dataSnapshot.hasChildren()) {
//                    for (final DataSnapshot child : dataSnapshot.getChildren()) {
//                        key = child.getKey();
//                        administrator = (boolean) child.child(StaticNames.ADMINISTRATOR).getValue();
//
//                        //Log.d("TESTING KEY", key);
//                        //We're going to fill out the userEvent object
//                        Query eventsQuery = mFirebaseReference.child(StaticNames.EVENTS).child(key);
//                        eventsQuery.addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                events = dataSnapshot.getValue(Events.class);
//
//                                Log.d("TESTING Title", events.getTitle());
//                            }
//
//                            @Override
//                            public void onCancelled(DatabaseError databaseError) {
//
//                            }
//                        });
//
//                        while (events == null) {
//                            //Do nothing
//                        }
//
//                    }
//                }
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//    }
}
