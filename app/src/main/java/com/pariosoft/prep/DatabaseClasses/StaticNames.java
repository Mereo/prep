package com.pariosoft.prep.DatabaseClasses;

/**
 * Created by mereo on 2017-01-16.
 */

public class StaticNames {
    //Constant variables
    public static final String USERS = "users";
    public static final String PHONENUMBER = "phoneNumber";
    public static final String EVENTS = "events";
    public static final String ADMINISTRATOR = "admnistrator";
    public static final String EventMember = "members";
    public static final String CHATMESSAGES = "chatMessages";
    public static final String MAP = "map";
    public static final String TIMESTAMP = "TIMESTAMP";
}
