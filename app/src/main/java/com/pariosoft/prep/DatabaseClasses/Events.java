package com.pariosoft.prep.DatabaseClasses;

import java.util.Date;

/**
 * Created by mereo on 2017-01-06.
 */

public class Events {
    // Class variables
    private String title;
    private String country;
    private String address;
    private String description;
    private String remarks;
    private Date date;
    private Time time;


    public Events() {
    }

    public Events(String title, String country, String address, String description, String remarks,
                  Date date, Time time) {
        this.title = title;
        this.country = country;
        this.address = address;
        this.description = description;
        this.remarks = remarks;
        this.date = date;
        this.time = time;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
