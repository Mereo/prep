package com.pariosoft.prep.DatabaseClasses;

/**
 * Created by mereo on 2017-01-15.
 */

public class UserEvent {
    private Events event;
    private boolean administrator;
    private String eventKey;

    public UserEvent() {

    }

    public UserEvent(Events event, boolean administrator, String eventKey) {
        this.event = event;
        this.administrator = administrator;
        this.eventKey = eventKey;
    }

    public Events getEvent() {
        return event;
    }

    public void setEvent(Events event) {
        this.event = event;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }
}