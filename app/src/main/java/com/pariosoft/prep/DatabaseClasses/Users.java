package com.pariosoft.prep.DatabaseClasses;

/**
 * Created by mereo on 2016-12-22.
 */

public class Users {
    private String uid;
    private String email;
    private String name;
    private String phoneNumber;

    public Users(String uid, String email, String name, String phoneNumber) {
        this.uid = uid;
        this.email = email;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public Users() {
        //Required for Datasnapshot.getValue(User.class)
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
