package com.pariosoft.prep.DatabaseClasses;

/**
 * Created by mereo on 2017-01-12.
 */

public class Time {
    int hour;
    int minute;

    public Time() {

    }

    public Time(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}

