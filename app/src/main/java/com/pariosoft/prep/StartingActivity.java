package com.pariosoft.prep;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.pariosoft.prep.Adapters.EventsAdapter;
import com.pariosoft.prep.DatabaseClasses.Events;
import com.pariosoft.prep.DatabaseClasses.StaticNames;
import com.pariosoft.prep.DatabaseClasses.UserEvent;
import com.pariosoft.prep.utils.CustomPreferences;

import java.util.ArrayList;
import java.util.List;

public class StartingActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {

    //General Fields
    private int REQUEST_CODE_PHONE_STATE;
    public static final String ANONYMOUS = "anonymous";
    private GoogleApiClient mGoogleApiClient;
    private String mUsername;

    private CustomPreferences preferences = CustomPreferences.getInstance();

    // Firebase instance variables
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mFirebaseReference;

    //Variables of Views
    private ProgressBar mEventProgressBar;
    private RecyclerView mEventRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private EventsAdapter eventsAdapter;

    //Variables of View toolkits
    private Toolbar toolbar;
    private FloatingActionMenu fom;
    private FloatingActionButton fob1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);
        //Initialisation of Views
        //mEventProgressBar = (ProgressBar) findViewById(R.id.eventProgressBar);
        mEventRecyclerView = (RecyclerView) findViewById(R.id.eventRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        toolbarFobFoM();

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        Log.d("TESTING USER", "The user is: " + mFirebaseAuth.toString());

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        Log.d("TESTING USER", "The user is: " + mFirebaseAuth.toString());

        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, (GoogleApiClient.OnConnectionFailedListener) this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        initialiseRecyclerAndProgressbarViews();
        getEvents();

    }

    //Private method for the FOB Menu
    private void toolbarFobFoM() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fom = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        fob1 = (FloatingActionButton) findViewById(R.id.floating_action_menu_item3);
        fob1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Snackbar.make(v, "TEST DE BOUTON DE EVENT!!!!!!!!!!!!", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent intent = new Intent(getBaseContext(), NewEventActivity.class);
                startActivity(intent);
                fom.toggle(false);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                mFirebaseAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                mUsername = ANONYMOUS;
                preferences.putPhoneNumber(StartingActivity.this, "");
                startActivity(new Intent(this, SignInActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initialiseRecyclerAndProgressbarViews() {
        //mEventProgressBar = (ProgressBar) findViewById(R.id.eventProgressBar);

    }

    private void getEvents() {
        mFirebaseReference = FirebaseDatabase.getInstance().getReference();
        final Query userEventQuery = mFirebaseReference;
        userEventQuery.addValueEventListener(new ValueEventListener() {
            String key;
            boolean administrator = false;
            //UserEvent userEvent;
            Events events = null;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //The list of events
                List<UserEvent> listUserEvents = new ArrayList<>();
                if (dataSnapshot.hasChildren()) {
                    for (final DataSnapshot child : dataSnapshot.child(StaticNames.USERS)
                            .child(mFirebaseUser.getUid())
                            .child(StaticNames.EVENTS)
                            .getChildren()) {
                        key = child.getKey();
                        administrator = (boolean) child.child(StaticNames.ADMINISTRATOR).getValue();
                        //Retrieve the Event info
                        events = dataSnapshot.child(StaticNames.EVENTS).child(key).getValue(Events.class);
                        Log.d("TESTING TITLE", mFirebaseUser.getDisplayName()+ ": "+ events.getTitle());
                        listUserEvents.add(0, new UserEvent(events,administrator,key));
                    }
                }
                eventsAdapter = new EventsAdapter(listUserEvents, getBaseContext());
                mEventRecyclerView.setAdapter(eventsAdapter);
                mEventRecyclerView.setLayoutManager(mLinearLayoutManager);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d("Later", "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }
}